import type {ProxyConfiguration} from "~/lib/api/museum/v1/images_pb";
import {createPromiseClient} from "@connectrpc/connect";
import {ImageProxyService} from "~/lib/api/museum/v1/images_connect";
import {createConnectTransport} from "@connectrpc/connect-web";

const useProxyConfigurationStore = defineStore('rpcProxyConfiguration', () => {
    const configurations = ref<ProxyConfiguration[]>([]);
    const names = computed(() => configurations.value.map(el => el.name));
    const loaded = ref(false);

    const client = computed(() =>
        createPromiseClient(ImageProxyService, createConnectTransport({
            baseUrl: useAppConfig().grpcHost,
        }))
    );

    const refresh = async (userToken: string) => {
        loaded.value = false;
        let response = await client.value.getProxyConfigurations({
            key: userToken,
        });

        configurations.value = response.configurations.map((el) => ({...el}) as ProxyConfiguration);
        loaded.value = true;
    };

    const init = async (userToken: string) => {
        if (configurations.value.length === 0) {
            await refresh(userToken);
        }
    }

    const save = async (configurations: ProxyConfiguration[], userToken: string) => {
        await client.value.configureDefaultProxies({
            key: userToken,
            configurations: configurations,
        });

        await refresh(userToken);
    }

    return {
        configurations,
        names,
        loaded,
        refresh,
        init,
        save,
    };
});

export default useProxyConfigurationStore;