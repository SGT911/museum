const useSessionStore = defineStore('session', () => {
    const userToken = useCookie('userToken', {
        encode(value) {
            if (value === undefined) {
                value = null;
            }

            return btoa(JSON.stringify(value));
        },
        decode(value) {
            let decodedValue = atob(value);
            return JSON.parse(decodedValue);
        },
        default() {
            return null;
        },
        path: '/',
        sameSite: "strict",
    });
    const mustUserToken = computed(() => userToken.value ?? 'null');

    const isLoggedIn = computed(() => userToken.value !== null && userToken.value !== undefined);

    const clear = () => {
        userToken.value = null;
    }

    return {
        userToken,
        mustUserToken,
        isLoggedIn,
        clear,
    };
});

export default useSessionStore;