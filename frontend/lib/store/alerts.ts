import {defineStore} from "pinia";

interface Alert {
    type: 'primary' | 'success' | 'warning' | 'danger';
    message: string;
}

function guidGenerator() {
    let S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

const useAlertStore = defineStore('alerts', () => {
    const alerts = ref<{ [k: string]: Alert }>({});

    const create = (alert: Alert) => {
        let id = guidGenerator();
        alerts.value = {...alerts.value, [id]: alert};

        return id;
    }

    const remove = (id: string) => {
        let value = alerts.value;
        if (value.hasOwnProperty(id)) {
            delete alerts.value[id];
        }

        alerts.value = value;
    }

    return {
        alerts,
        create,
        remove,
    };
});


export default useAlertStore;