package models

import (
	adminv1 "gitlab.com/SGT911/museum/backend/gen/museum/admin/v1"
	museumv1 "gitlab.com/SGT911/museum/backend/gen/museum/v1"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PixelConfiguration struct {
	X int32 `json:"x" bson:"x"`
	Y int32 `json:"y" bson:"y"`
}

type ImageProxyConfiguration struct {
	ID                 primitive.ObjectID      `json:"id" bson:"_id"`
	KeyID              primitive.ObjectID      `json:"key_id" bson:"key_id"`
	Name               string                  `json:"name" bson:"name"`
	Type               museumv1.ImageProxyType `json:"type" bson:"type"`
	PixelConfiguration *PixelConfiguration     `json:"pixel_configuration" bson:"pixel_configuration"`
	RatioConfiguration *float32                `json:"ratio_configuration" bson:"ratio_configuration"`
}

type Image struct {
	ID         primitive.ObjectID      `json:"id" bson:"_id"`
	KeyID      primitive.ObjectID      `json:"key_id" bson:"key_id"`
	BucketType adminv1.RestrictionType `json:"bucket_type" bson:"bucket_type"`
	MimeType   string                  `json:"mime_type" bson:"mime_type"`
	Hash       []byte                  `json:"hash" bson:"hash"`
	Proxies    []string                `json:"proxies" bson:"proxies"`
}
