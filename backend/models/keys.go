package models

import (
	adminv1 "gitlab.com/SGT911/museum/backend/gen/museum/admin/v1"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type KeyRestrictions struct {
	Type     adminv1.RestrictionType `bson:"type" json:"type"`
	MaxItems int                     `bson:"maxItems" json:"maxItems"`
}

type Key struct {
	ID      primitive.ObjectID `bson:"_id" json:"id"`
	Enabled bool               `bson:"enabled" json:"enabled"`

	Key   string `bson:"key" json:"key"`
	Label string `bson:"label" json:"label"`

	Restrictions []KeyRestrictions `bson:"restrictions" json:"restrictions"`
}
