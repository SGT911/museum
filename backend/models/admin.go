package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AdminToken struct {
	ID    primitive.ObjectID `bson:"_id" json:"id"`
	Token string             `bson:"token" json:"token"`
	Label string             `bson:"label" json:"label"`
}
