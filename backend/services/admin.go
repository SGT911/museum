package services

import (
	"connectrpc.com/connect"
	"context"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	adminv1 "gitlab.com/SGT911/museum/backend/gen/museum/admin/v1"
	"gitlab.com/SGT911/museum/backend/gen/museum/admin/v1/adminv1connect"
	"gitlab.com/SGT911/museum/backend/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/protobuf/types/known/emptypb"
	"log/slog"
	"time"
)

type AdminAuthService struct {
	*adminv1connect.UnimplementedAdminAuthServiceHandler

	TokenSecret []byte
	DB          *mongo.Database
}

type AdminKeyService struct {
	*adminv1connect.UnimplementedAdminKeyServiceHandler

	TokenSecret []byte
	DB          *mongo.Database
}

const (
	AdminTokensCollection = "admin_tokens"
	KeysCollection        = "keys"
)

func (s *AdminAuthService) GetToken(ctx context.Context, request *connect.Request[adminv1.GetTokenRequest]) (*connect.Response[adminv1.GetTokenResponse], error) {
	collection := s.DB.Collection(AdminTokensCollection)
	slog.Info("Searching for token", "token", request.Msg.GetAuthToken())
	result := collection.FindOne(ctx, bson.M{
		"token": request.Msg.GetAuthToken(),
	})

	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return nil, connect.NewError(connect.CodeNotFound, errors.New("token was not found"))
	}

	if result.Err() != nil {
		return nil, connect.NewError(connect.CodeInternal, result.Err())
	}

	token := models.AdminToken{}
	if err := result.Decode(&token); err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	tokenTime := time.Now()
	unsignedToken := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"sub": token.ID,
		"nbf": tokenTime.Unix(),
		"iat": tokenTime.Unix(),
		"exp": tokenTime.Add(time.Duration(request.Msg.GetTtl()) * time.Second).Unix(),
	})

	slog.Debug("Token created", "token_subject", token.ID, "valid_until", unsignedToken.Claims.(jwt.MapClaims)["exp"])

	if signedToken, err := unsignedToken.SignedString(s.TokenSecret); err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	} else {
		return connect.NewResponse(&adminv1.GetTokenResponse{
			Token: signedToken,
		}), nil
	}
}

func (s *AdminAuthService) RefreshToken(ctx context.Context, request *connect.Request[adminv1.RefreshTokenRequest]) (*connect.Response[adminv1.RefreshTokenResponse], error) {
	token, err := jwt.Parse(request.Msg.Token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return s.TokenSecret, nil
	})

	if err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, err)
	}

	collection := s.DB.Collection(AdminTokensCollection)

	var subject string
	if subject, err = token.Claims.GetSubject(); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token structure"))
	}

	var id primitive.ObjectID
	if id, err = primitive.ObjectIDFromHex(subject); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token structure"))
	}

	slog.Info("Refreshing token", "token", id)

	result := collection.FindOne(ctx, bson.M{
		"_id": id,
	})

	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return nil, connect.NewError(connect.CodeNotFound, errors.New("token was not found or is not valid"))
	}

	if result.Err() != nil {
		return nil, connect.NewError(connect.CodeInternal, result.Err())
	}

	iat, err := token.Claims.GetIssuedAt()
	if err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token structure"))
	}
	exp, err := token.Claims.GetExpirationTime()
	if err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token structure"))
	}

	diff := exp.Time.Sub(iat.Time)
	tokenTime := time.Now()
	unsignedToken := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"sub": id,
		"nbf": tokenTime.Unix(),
		"iat": tokenTime.Unix(),
		"exp": tokenTime.Add(diff).Unix(),
	})

	slog.Debug("Token created", "token_subject", id, "valid_until", unsignedToken.Claims.(jwt.MapClaims)["exp"])

	if signedToken, err := unsignedToken.SignedString(s.TokenSecret); err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	} else {
		return connect.NewResponse(&adminv1.RefreshTokenResponse{
			Token: signedToken,
		}), nil
	}
}

func (s *AdminKeyService) validateAuthToken(ctx context.Context, tokenString string) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return s.TokenSecret, nil
	})

	if err != nil {
		return err
	}

	collection := s.DB.Collection(AdminTokensCollection)

	var subject string
	if subject, err = token.Claims.GetSubject(); err != nil {
		return errors.New("invalid token structure")
	}

	var id primitive.ObjectID
	if id, err = primitive.ObjectIDFromHex(subject); err != nil {
		return errors.New("invalid token structure")
	}

	result := collection.FindOne(ctx, bson.M{
		"_id": id,
	})

	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return errors.New("token was not found or is not valid")
	}

	if result.Err() != nil {
		return result.Err()
	}

	return nil
}

func (s *AdminKeyService) CreateKey(ctx context.Context, request *connect.Request[adminv1.CreateKeyRequest]) (*connect.Response[adminv1.CreateKeyResponse], error) {
	if err := s.validateAuthToken(ctx, request.Msg.GetAuthToken()); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token"))
	}

	collection := s.DB.Collection(KeysCollection)
	key := CreateRandomString(24)

	keyModel := models.Key{
		ID:      primitive.NewObjectID(),
		Key:     key,
		Label:   request.Msg.GetLabel(),
		Enabled: true,
		Restrictions: []models.KeyRestrictions{
			{
				Type:     adminv1.RestrictionType_RESTRICTION_TYPE_PUBLIC,
				MaxItems: -1,
			},
			{
				Type:     adminv1.RestrictionType_RESTRICTION_TYPE_PRIVATE,
				MaxItems: -1,
			},
		},
	}

	if _, err := collection.InsertOne(ctx, keyModel); err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&adminv1.CreateKeyResponse{
		Key: key,
		Restrictions: []*adminv1.Restriction{
			{
				Type:     adminv1.RestrictionType_RESTRICTION_TYPE_PUBLIC,
				MaxItems: -1,
			},
			{
				Type:     adminv1.RestrictionType_RESTRICTION_TYPE_PRIVATE,
				MaxItems: -1,
			},
		},
	}), nil
}

func (s *AdminKeyService) SetKeyRestrictions(ctx context.Context, request *connect.Request[adminv1.SetKeyRestrictionsRequest]) (*connect.Response[emptypb.Empty], error) {
	if err := s.validateAuthToken(ctx, request.Msg.GetAuthToken()); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token"))
	}

	collection := s.DB.Collection(KeysCollection)
	result := collection.FindOne(ctx, bson.M{
		"key": request.Msg.GetKey(),
	})
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return nil, connect.NewError(connect.CodeNotFound, errors.New("key not found"))
	}

	if result.Err() != nil {
		return nil, connect.NewError(connect.CodeInternal, result.Err())
	}

	restrictions := make([]models.KeyRestrictions, 0)
	for _, restriction := range request.Msg.GetRestrictions() {
		restrictions = append(restrictions, models.KeyRestrictions{
			Type:     restriction.GetType(),
			MaxItems: int(restriction.GetMaxItems()),
		})
	}

	_, err := collection.UpdateOne(ctx, bson.M{
		"key": request.Msg.GetKey(),
	}, bson.M{
		"$set": bson.M{
			"restrictions": restrictions,
		},
	})
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&emptypb.Empty{}), nil
}

func (s *AdminKeyService) DisableKey(ctx context.Context, request *connect.Request[adminv1.DisableKeyRequest]) (*connect.Response[emptypb.Empty], error) {
	if err := s.validateAuthToken(ctx, request.Msg.GetAuthToken()); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token"))
	}

	collection := s.DB.Collection(KeysCollection)
	result := collection.FindOne(ctx, bson.M{
		"key": request.Msg.GetKey(),
	})
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return nil, connect.NewError(connect.CodeNotFound, errors.New("key not found"))
	}

	if result.Err() != nil {
		return nil, connect.NewError(connect.CodeInternal, result.Err())
	}

	_, err := collection.UpdateOne(ctx, bson.M{
		"key": request.Msg.GetKey(),
	}, bson.M{
		"$set": bson.M{
			"enabled": false,
		},
	})
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&emptypb.Empty{}), nil
}

func (s *AdminKeyService) EnableKey(ctx context.Context, request *connect.Request[adminv1.EnableKeyRequest]) (*connect.Response[emptypb.Empty], error) {
	if err := s.validateAuthToken(ctx, request.Msg.GetAuthToken()); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token"))
	}

	collection := s.DB.Collection(KeysCollection)
	result := collection.FindOne(ctx, bson.M{
		"key": request.Msg.GetKey(),
	})
	if errors.Is(result.Err(), mongo.ErrNoDocuments) {
		return nil, connect.NewError(connect.CodeNotFound, errors.New("key not found"))
	}

	if result.Err() != nil {
		return nil, connect.NewError(connect.CodeInternal, result.Err())
	}

	_, err := collection.UpdateOne(ctx, bson.M{
		"key": request.Msg.GetKey(),
	}, bson.M{
		"$set": bson.M{
			"enabled": true,
		},
	})
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&emptypb.Empty{}), nil
}

func (s *AdminKeyService) GetKey(ctx context.Context, request *connect.Request[adminv1.GetKeyRequest]) (*connect.Response[adminv1.GetKeyResponse], error) {
	if err := s.validateAuthToken(ctx, request.Msg.GetAuthToken()); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token"))
	}

	collection := s.DB.Collection(KeysCollection)
	result := collection.FindOne(ctx, bson.M{
		"key": request.Msg.GetKey(),
	})

	modelKey := models.Key{}
	err := result.Decode(&modelKey)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	restrictions := make([]*adminv1.Restriction, 0)
	for _, restriction := range modelKey.Restrictions {
		restrictions = append(restrictions, &adminv1.Restriction{
			Type:     restriction.Type,
			MaxItems: int32(restriction.MaxItems),
		})
	}

	return connect.NewResponse(&adminv1.GetKeyResponse{
		Key:          modelKey.Key,
		Label:        modelKey.Label,
		Enabled:      modelKey.Enabled,
		Restrictions: restrictions,
	}), nil
}

func (s *AdminKeyService) ListKeys(ctx context.Context, request *connect.Request[adminv1.ListKeysRequest]) (*connect.Response[adminv1.ListKeysResponse], error) {
	if err := s.validateAuthToken(ctx, request.Msg.GetAuthToken()); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid token"))
	}

	collection := s.DB.Collection(KeysCollection)
	criteria := make([]bson.M, 0)
	if request.Msg.GetRegexKey() != "" {
		criteria = append(criteria, bson.M{
			"key": bson.M{
				"$regex":   request.Msg.GetRegexKey(),
				"$options": "iu",
			},
		})
	}

	if request.Msg.GetRegexLabel() != "" {
		criteria = append(criteria, bson.M{
			"label": bson.M{
				"$regex":   request.Msg.GetRegexLabel(),
				"$options": "iu",
			},
		})
	}

	searchCriteria := bson.M{}
	if len(criteria) > 0 {
		searchCriteria = bson.M{
			"$or": criteria,
		}
	}

	hits, err := collection.CountDocuments(ctx, searchCriteria)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	totalCount, err := collection.CountDocuments(ctx, bson.M{})
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	findOptions := options.Find().
		SetSkip(int64(request.Msg.GetOffset())).
		SetLimit(int64(request.Msg.GetLimit())).
		SetProjection(bson.M{
			"_id": "$key",
		})

	cursor, err := collection.Find(ctx, searchCriteria, findOptions)
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		_ = cursor.Close(ctx)
	}(cursor, ctx)

	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	results := make([]struct {
		Key string `bson:"_id"`
	}, 0)
	err = cursor.All(ctx, &results)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	keys := make([]string, 0)
	for _, result := range results {
		keys = append(keys, result.Key)
	}

	return connect.NewResponse(&adminv1.ListKeysResponse{
		Hits:       uint64(hits),
		TotalCount: uint64(totalCount),
		Keys:       keys,
	}), nil
}
