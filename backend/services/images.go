package services

import (
	"connectrpc.com/connect"
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/minio/minio-go/v7"
	"github.com/nats-io/nats.go"
	adminv1 "gitlab.com/SGT911/museum/backend/gen/museum/admin/v1"
	museumv1 "gitlab.com/SGT911/museum/backend/gen/museum/v1"
	"gitlab.com/SGT911/museum/backend/gen/museum/v1/museumv1connect"
	"gitlab.com/SGT911/museum/backend/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log/slog"
	"net/url"
	"time"
)

const ImageProxyConfigurationsCollection = "image_proxy_configurations"
const ImagesCollection = "images"

type ImageService struct {
	*museumv1connect.UnimplementedImageServiceHandler
	*museumv1connect.UnimplementedImageProxyServiceHandler

	PublicBucket  string
	PrivateBucket string

	DB *mongo.Database
	S3 *minio.Client

	Nats *nats.Conn

	backgroundExitChannels []chan bool
}

func (s *ImageService) validateKey(ctx context.Context, key string) (primitive.ObjectID, error) {
	collection := s.DB.Collection(KeysCollection)

	result := collection.FindOne(ctx, bson.M{"key": key})
	if result.Err() != nil {
		return primitive.NilObjectID, result.Err()
	}

	model := models.Key{}
	if err := result.Decode(&model); err != nil {
		return primitive.NilObjectID, err
	}

	return model.ID, nil
}

func (s *ImageService) GetProxyConfigurations(ctx context.Context, request *connect.Request[museumv1.GetProxyConfigurationsRequest]) (*connect.Response[museumv1.GetProxyConfigurationsResponse], error) {
	keyId, err := s.validateKey(ctx, request.Msg.GetKey())
	if err != nil {
		slog.Warn("Invalid key", "key", request.Msg.GetKey(), "error", err, "addressOrigin", request.Peer().Addr)

		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid key"))
	}

	collection := s.DB.Collection(ImageProxyConfigurationsCollection)
	configurations, err := collection.Find(ctx, bson.M{"key_id": keyId})
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	names := make([]string, 0)
	exportedConfigurations := make([]*museumv1.ProxyConfiguration, 0)
	for configurations.Next(ctx) {
		internalConfiguration := models.ImageProxyConfiguration{}
		if err := configurations.Decode(&internalConfiguration); err != nil {
			return nil, connect.NewError(connect.CodeInternal, err)
		}

		names = append(names, internalConfiguration.Name)
		configuration := museumv1.ProxyConfiguration{
			Name: internalConfiguration.Name,
			Type: internalConfiguration.Type,
		}

		switch configuration.Type {
		case museumv1.ImageProxyType_IMAGE_PROXY_TYPE_RATIO:
			configuration.Ratio = *internalConfiguration.RatioConfiguration
			break
		case museumv1.ImageProxyType_IMAGE_PROXY_TYPE_PIXELS:
			configuration.XLength = internalConfiguration.PixelConfiguration.X
			configuration.YLength = internalConfiguration.PixelConfiguration.Y
			break
		}

		exportedConfigurations = append(exportedConfigurations, &configuration)
	}

	return connect.NewResponse(&museumv1.GetProxyConfigurationsResponse{
		Names:          names,
		Configurations: exportedConfigurations,
	}), nil
}

func (s *ImageService) ConfigureDefaultProxies(ctx context.Context, request *connect.Request[museumv1.ConfigureDefaultProxiesRequest]) (*connect.Response[museumv1.ConfigureDefaultProxiesResponse], error) {
	keyId, err := s.validateKey(ctx, request.Msg.GetKey())
	if err != nil {
		slog.Warn("Invalid key", "key", request.Msg.GetKey(), "error", err, "addressOrigin", request.Peer().Addr)

		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid key"))
	}

	collection := s.DB.Collection(ImageProxyConfigurationsCollection)
	_, _ = collection.DeleteMany(ctx, bson.M{"key_id": keyId})

	proxyNames := make([]string, 0)
	for _, configuration := range request.Msg.GetConfigurations() {
		if configuration.GetName() == "original" {
			slog.Warn("Trying to set proxy configuration", "configuration", configuration, "keyId", request.Msg.GetKey())
			continue
		}

		proxyNames = append(proxyNames, configuration.GetName())
		proxyConfiguration := models.ImageProxyConfiguration{
			ID:    primitive.NewObjectID(),
			KeyID: keyId,
			Name:  configuration.GetName(),
			Type:  configuration.GetType(),
		}

		switch proxyConfiguration.Type {
		case museumv1.ImageProxyType_IMAGE_PROXY_TYPE_PIXELS:
			proxyConfiguration.PixelConfiguration = &models.PixelConfiguration{
				X: configuration.GetXLength(),
				Y: configuration.GetYLength(),
			}
			break
		case museumv1.ImageProxyType_IMAGE_PROXY_TYPE_RATIO:
			value := configuration.GetRatio()
			proxyConfiguration.RatioConfiguration = &value
		default:
			return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid image proxy type"))
		}

		_, err = collection.InsertOne(ctx, proxyConfiguration)
		if err != nil {
			return nil, err
		}
	}

	slog.Debug("Loading proxy configurations", "proxyNames", proxyNames, "keyId", request.Msg.GetKey())

	_ = s.Nats.Publish("image.proxy.update", []byte(keyId.Hex()))

	return connect.NewResponse(&museumv1.ConfigureDefaultProxiesResponse{
		ProxyNames: proxyNames,
	}), nil
}

func (s *ImageService) UploadImage(ctx context.Context, request *connect.Request[museumv1.UploadImageRequest], streamResponse *connect.ServerStream[museumv1.UploadImageResponse]) error {
	keyId, err := s.validateKey(ctx, request.Msg.GetKey())
	if err != nil {
		slog.Warn("Invalid key", "key", request.Msg.GetKey(), "error", err, "addressOrigin", request.Peer().Addr)

		return connect.NewError(connect.CodeInvalidArgument, errors.New("invalid key"))
	}

	keysCollection := s.DB.Collection(KeysCollection)
	result := keysCollection.FindOne(ctx, bson.M{
		"_id": keyId,
	})
	keyModel := &models.Key{}
	if err = result.Decode(keyModel); err != nil {
		return connect.NewError(connect.CodeInternal, err)
	}

	restrictionCount := int64(-2)
	for _, restriction := range keyModel.Restrictions {
		if restriction.Type == request.Msg.GetRestriction() {
			restrictionCount = int64(restriction.MaxItems)
			break
		}
	}

	collection := s.DB.Collection(ImagesCollection)
	if restrictionCount != -1 {
		count, err := collection.CountDocuments(ctx, bson.M{
			"bucket_type": request.Msg.GetRestriction(),
			"key":         keyId,
		})

		if err != nil && !errors.Is(err, mongo.ErrNoDocuments) {
			return connect.NewError(connect.CodeInternal, err)
		}

		if restrictionCount <= count {
			return connect.NewError(connect.CodeFailedPrecondition, errors.New("the key exceeded the limit of objects"))
		}
	}

	objectId := primitive.NewObjectID()
	bucket := ""
	switch request.Msg.GetRestriction() {
	case adminv1.RestrictionType_RESTRICTION_TYPE_PUBLIC:
		bucket = s.PublicBucket
		break
	case adminv1.RestrictionType_RESTRICTION_TYPE_PRIVATE:
		bucket = s.PublicBucket
		break
	default:
		return connect.NewError(connect.CodeInvalidArgument, errors.New("invalid restriction"))
	}

	timeout := 5 * time.Minute

	filename := fmt.Sprintf("%s-%s", keyId.Hex(), objectId.Hex())
	objectUrl, err := s.S3.PresignedPutObject(ctx, bucket, filename, timeout)
	if err != nil {
		return connect.NewError(connect.CodeInternal, err)
	}

	slog.Info("Uploaded image", "bucket", bucket, "filename", filename)
	slog.Debug("Image upload url pre-signed", "objectId", objectId, "keyId", request.Msg.GetKey(), "filename", filename, "timeout", timeout)

	if err = streamResponse.Send(&museumv1.UploadImageResponse{
		Status:    museumv1.UploadImageStatus_UPLOAD_IMAGE_STATUS_URL_SIGNED,
		TargetUrl: objectUrl.String(),
		Id:        objectId.Hex(),
	}); err != nil {
		return connect.NewError(connect.CodeInternal, err)
	}

	if err = streamResponse.Send(&museumv1.UploadImageResponse{
		Status: museumv1.UploadImageStatus_UPLOAD_IMAGE_STATUS_UPLOADING,
		Id:     objectId.Hex(),
	}); err != nil {
		return connect.NewError(connect.CodeInternal, err)
	}

	slog.Debug("Awaiting for object upload", "objectId", objectId, "keyId", request.Msg.GetKey(), "bucket", bucket, "filename", filename, "timeout", timeout)

	start := time.Now()
	var stats minio.ObjectInfo
	for {
		stats, err = s.S3.StatObject(ctx, bucket, filename, minio.StatObjectOptions{})

		if err != nil && time.Since(start) > timeout {
			slog.Warn("Object upload is timeout", "objectId", objectId, "keyId", request.Msg.GetKey(), "bucket", bucket, "filename", filename)
			return connect.NewError(connect.CodeInternal, errors.New("upload timed out"))
		} else if err == nil {
			break
		}

		time.Sleep(100 * time.Millisecond)
	}

	model := &models.Image{
		ID:         objectId,
		BucketType: request.Msg.GetRestriction(),
		KeyID:      keyId,
		MimeType:   request.Msg.GetMime(),
		Proxies:    make([]string, 0),
	}

	model.Hash, err = hex.DecodeString(stats.ETag)
	if err != nil {
		return connect.NewError(connect.CodeInternal, err)
	}

	_, err = collection.InsertOne(ctx, model)
	if err = streamResponse.Send(&museumv1.UploadImageResponse{
		Status: museumv1.UploadImageStatus_UPLOAD_IMAGE_STATUS_UPLOAD_COMPLETE,
		Id:     objectId.Hex(),
	}); err != nil {
		return connect.NewError(connect.CodeInternal, err)
	}

	slog.Info("Image uploaded", "objectId", objectId, "keyId", request.Msg.GetKey(), "bucket", bucket, "filename", filename)

	_ = s.Nats.Publish("image.upload", []byte(objectId.Hex()))

	return nil
}

func (s *ImageService) GetImageUrl(ctx context.Context, request *connect.Request[museumv1.GetImageUrlRequest]) (*connect.Response[museumv1.GetImageUrlResponse], error) {
	keyId, err := s.validateKey(ctx, request.Msg.GetKey())
	if err != nil {
		slog.Warn("Invalid key", "key", request.Msg.GetKey(), "error", err, "addressOrigin", request.Peer().Addr)

		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid key"))
	}

	objectId, err := primitive.ObjectIDFromHex(request.Msg.GetObjectId())
	if err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid objectId"))
	}

	collection := s.DB.Collection(ImagesCollection)
	result := collection.FindOne(ctx, bson.M{
		"_id":    objectId,
		"key_id": keyId,
	})

	if result.Err() != nil {
		if errors.Is(result.Err(), mongo.ErrNoDocuments) {
			return nil, connect.NewError(connect.CodeNotFound, errors.New("image not found"))
		}
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	modelImage := models.Image{}
	if err := result.Decode(&modelImage); err != nil {
		return nil, connect.NewError(connect.CodeInvalidArgument, err)
	}

	bucket := ""
	switch modelImage.BucketType {
	case adminv1.RestrictionType_RESTRICTION_TYPE_PUBLIC:
		bucket = s.PublicBucket
		break
	case adminv1.RestrictionType_RESTRICTION_TYPE_PRIVATE:
		bucket = s.PublicBucket
		break
	default:
		return nil, connect.NewError(connect.CodeInvalidArgument, errors.New("invalid restriction"))
	}

	timeout := 30 * time.Second

	if request.Msg.GetProxyConfiguration() == "original" {
		filename := fmt.Sprintf("%s-%s", modelImage.KeyID.Hex(), modelImage.ID.Hex())
		imageUrl, err := s.S3.PresignedGetObject(ctx, bucket, filename, timeout, url.Values{})
		if err != nil {
			return nil, connect.NewError(connect.CodeInternal, err)
		}

		return connect.NewResponse(&museumv1.GetImageUrlResponse{
			Url: imageUrl.String(),
		}), nil
	}

	collection = s.DB.Collection(ImageProxyConfigurationsCollection)
	result = collection.FindOne(ctx, bson.M{
		"key_id": keyId,
		"name":   request.Msg.GetProxyConfiguration(),
	})

	if result.Err() != nil {
		if errors.Is(result.Err(), mongo.ErrNoDocuments) {
			return nil, connect.NewError(connect.CodeNotFound, errors.New("proxy configuration not found"))
		}
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	filename := fmt.Sprintf("proxy/%s-%s-%s", request.Msg.GetProxyConfiguration(), modelImage.KeyID.Hex(), modelImage.ID.Hex())
	imageUrl, err := s.S3.PresignedGetObject(ctx, bucket, filename, timeout, url.Values{})
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&museumv1.GetImageUrlResponse{
		Url: imageUrl.String(),
	}), nil
}
