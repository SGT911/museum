package services

import (
	"connectrpc.com/connect"
	"context"
	museumv1 "gitlab.com/SGT911/museum/backend/gen/museum/v1"
	"gitlab.com/SGT911/museum/backend/gen/museum/v1/museumv1connect"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/protobuf/types/known/emptypb"
	"log/slog"
	"time"
)

type PingService struct {
	*museumv1connect.UnimplementedPingServiceHandler
}

type AuthService struct {
	*museumv1connect.UnimplementedAuthServiceHandler

	DB *mongo.Database
}

func (s *PingService) Ping(context.Context, *connect.Request[emptypb.Empty]) (*connect.Response[museumv1.PingResponse], error) {
	slog.Debug("Ping was triggered")
	return connect.NewResponse(&museumv1.PingResponse{
		Message: "pong",
	}), nil
}

func (s *PingService) GetTimestamp(context.Context, *connect.Request[emptypb.Empty]) (*connect.Response[museumv1.GetTimestampResponse], error) {
	now := time.Now().Local()
	_, zoneOffset := now.Zone()
	slog.Debug("Timestamp request", "time", now, "unix", now.UnixMilli())
	return connect.NewResponse(&museumv1.GetTimestampResponse{
		Timestamp: uint64(now.UnixMilli()),
		Offset:    int32(zoneOffset),
	}), nil
}

func (s *AuthService) TestTokenAuth(ctx context.Context, request *connect.Request[museumv1.TestTokenAuthRequest]) (*connect.Response[museumv1.TestTokenAuthResponse], error) {
	collection := s.DB.Collection(KeysCollection)
	count, err := collection.CountDocuments(ctx, bson.M{
		"key": request.Msg.GetKey(),
	})

	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&museumv1.TestTokenAuthResponse{
		Ok: count == 1,
	}), nil
}
