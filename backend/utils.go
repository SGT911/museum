package main

import (
	"context"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/nats-io/nats.go"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

func GetEnvOrDefault(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

func GetDatabase() (database *mongo.Database, err error) {
	uri := GetEnvOrDefault("MONGODB_URI", "mongodb://localhost:27017")
	dbName := GetEnvOrDefault("MONGODB_NAME", "museum")

	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	opts := options.Client().ApplyURI(uri).SetServerAPIOptions(serverAPI)
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		return nil, err
	}

	if err = client.Ping(context.TODO(), nil); err != nil {
		return nil, err
	}

	database = client.Database(dbName)
	return
}

func GetS3Client() (client *minio.Client, err error) {
	return minio.New(GetEnvOrDefault("S3_ENDPOINT", "localhost:9000"), &minio.Options{
		Creds: credentials.NewStaticV4(
			GetEnvOrDefault("S3_ACCESS_KEY_ID", "minioadmin"),
			GetEnvOrDefault("S3_ACCESS_SECRET_ID", "minioadmin"),
			"",
		),
		Secure: GetEnvOrDefault("S3_SECURE", "false") == "true",
		Region: GetEnvOrDefault("S3_REGION", "us-east-1"),
	})
}

func GetNatsClient() (*nats.Conn, error) {
	return nats.Connect(
		GetEnvOrDefault("NATS_URL", "nats://localhost:4222"),
		nats.PingInterval(60*time.Second))
}
