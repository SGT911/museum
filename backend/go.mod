module gitlab.com/SGT911/museum/backend

go 1.22.3

require (
	connectrpc.com/connect v1.16.2
	connectrpc.com/cors v0.1.0
	connectrpc.com/grpcreflect v1.2.0
	github.com/anthonynsimon/bild v0.13.0
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/joho/godotenv v1.5.1
	github.com/minio/minio-go/v7 v7.0.70
	github.com/nats-io/nats.go v1.35.0
	github.com/nickalie/go-webpbin v0.0.0-20220110095747-f10016bf2dc1
	github.com/rs/cors v1.11.0
	go.mongodb.org/mongo-driver v1.15.0
	golang.org/x/net v0.23.0
	google.golang.org/protobuf v1.34.1
)

require (
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/frankban/quicktest v1.14.6 // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/klauspost/compress v1.17.6 // indirect
	github.com/klauspost/cpuid/v2 v2.2.6 // indirect
	github.com/mholt/archiver v3.1.1+incompatible // indirect
	github.com/minio/md5-simd v1.1.2 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/nickalie/go-binwrapper v0.0.0-20190114141239-525121d43c84 // indirect
	github.com/nwaples/rardecode v1.1.0 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/rs/xid v1.5.0 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
