package workers

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/anthonynsimon/bild/transform"
	"github.com/minio/minio-go/v7"
	"github.com/nats-io/nats.go"
	"github.com/nickalie/go-webpbin"
	adminv1 "gitlab.com/SGT911/museum/backend/gen/museum/admin/v1"
	museumv1 "gitlab.com/SGT911/museum/backend/gen/museum/v1"
	"gitlab.com/SGT911/museum/backend/models"
	"gitlab.com/SGT911/museum/backend/services"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"image"
	"log/slog"
	"time"
)

type ImageServiceWorker struct {
	PublicBucket  string
	PrivateBucket string

	DB *mongo.Database
	S3 *minio.Client

	Nats *nats.Conn

	backgroundExitChannels []chan bool
}

func (s *ImageServiceWorker) ForkBackgroundProcess() error {
	s.backgroundExitChannels = make([]chan bool, 0)

	processChannel := make(chan bool, 1)
	go func(exitChannel <-chan bool, db *mongo.Database, mq *nats.Conn, s3 *minio.Client) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		channel := make(chan *nats.Msg)
		subscribeUpload, err := mq.ChanSubscribe("image.upload", channel)
		if err != nil {
			panic(err)
		}

		subscribeGenerateProxy, err := mq.ChanSubscribe("image.generate-proxy", channel)
		if err != nil {
			panic(err)
		}

		_ = subscribeUpload.SetPendingLimits(1000, 10*1024)
		_ = subscribeGenerateProxy.SetPendingLimits(1000, 10*1024)

		for {
			select {
			case message := <-channel:
				objectId, _ := primitive.ObjectIDFromHex(string(message.Data))

				slog.Debug("Message image uploaded", "objectId", objectId)
				collection := db.Collection(services.ImagesCollection)

				result := collection.FindOne(ctx, bson.M{
					"_id": objectId,
				})

				if result.Err() != nil && !errors.Is(result.Err(), mongo.ErrNoDocuments) {
					slog.Warn("Error while loading the image from database", "objectId", objectId)
					_ = message.NakWithDelay(500 * time.Millisecond)
					break
				}

				imageModel := models.Image{}
				err = result.Decode(&imageModel)
				if err != nil {
					slog.Warn("Error while loading the image from database", "objectId", objectId)
					_ = message.NakWithDelay(500 * time.Millisecond)
					break
				}

				bucket := ""
				switch imageModel.BucketType {
				case adminv1.RestrictionType_RESTRICTION_TYPE_PUBLIC:
					bucket = s.PublicBucket
					break
				case adminv1.RestrictionType_RESTRICTION_TYPE_PRIVATE:
					bucket = s.PublicBucket
					break
				}

				filename := fmt.Sprintf("%s-%s", imageModel.KeyID.Hex(), imageModel.ID.Hex())

				slog.Debug("Getting content from S3", "objectId", objectId)

				response, err := s3.GetObject(ctx, bucket, filename, minio.GetObjectOptions{})
				if err != nil {
					slog.Warn("Error while loading the image from S3", "objectId", objectId)
					_ = message.NakWithDelay(500 * time.Millisecond)
					break
				}

				responseAttrs, _ := s3.GetObjectAttributes(ctx, bucket, filename, minio.ObjectAttributesOptions{})

				proxyCollections := db.Collection(services.ImageProxyConfigurationsCollection)
				proxyCursor, err := proxyCollections.Find(ctx, bson.M{
					"key_id": imageModel.KeyID,
				})

				if err != nil {
					slog.Warn("Error while getting proxy configurations from database", "objectId", imageModel.KeyID)
					_ = message.NakWithDelay(500 * time.Millisecond)
					break
				}

				imgBuffer, _, err := image.Decode(response)
				if err != nil {
					slog.Error("Error while decoding the image", "objectId", objectId)
					break
				}

				_, err = response.Seek(0, 0)
				if err != nil {
					slog.Warn("Error while seeking image", "objectId", objectId)
					break
				}

				imgConfig, _, err := image.DecodeConfig(response)
				if err != nil {
					slog.Error("Error while decoding the image", "objectId", objectId)
					break
				}

				_, err = response.Seek(0, 0)
				if err != nil {
					slog.Warn("Error while seeking image", "objectId", objectId)
					break
				}

				for proxyCursor.Next(ctx) {
					configuration := models.ImageProxyConfiguration{}
					err = proxyCursor.Decode(&configuration)
					if err != nil {
						slog.Warn("Error decoding a configuration from", "objectId", objectId)
						continue
					}

					var target *image.RGBA = nil
					switch configuration.Type {
					case museumv1.ImageProxyType_IMAGE_PROXY_TYPE_RATIO:
						width := float32(imgConfig.Width) * *configuration.RatioConfiguration
						height := float32(imgConfig.Height) * *configuration.RatioConfiguration
						target = transform.Resize(imgBuffer, int(width), int(height), transform.Gaussian)
						break
					case museumv1.ImageProxyType_IMAGE_PROXY_TYPE_PIXELS:
						var width, height float32
						if configuration.PixelConfiguration.Y == 0 {
							width = float32(configuration.PixelConfiguration.X)
							height = float32(imgConfig.Height) * (width / float32(imgConfig.Width))
						} else {
							height = float32(configuration.PixelConfiguration.Y)
							width = float32(imgConfig.Width) * (height / float32(imgConfig.Height))
						}
						target = transform.Resize(imgBuffer, int(width), int(height), transform.Gaussian)
						break
					}

					if target == nil {
						slog.Warn("Unknown proxy type", "type", configuration.Type, "keyId", configuration.KeyID, "configuration", configuration.Name)
						continue
					}

					buf := bytes.Buffer{}
					err = webpbin.Encode(&buf, target)
					if err != nil {
						slog.Error("Error while encoding the image", "objectId", objectId, "type", configuration.Type, "keyId", configuration.KeyID, "configuration", configuration.Name)
						continue
					}

					_, err = s3.PutObject(ctx, bucket, fmt.Sprintf("proxy/%s-%s", configuration.Name, filename), &buf, int64(buf.Len()), minio.PutObjectOptions{
						ContentType: "image/webp",
						UserMetadata: map[string]string{
							"Canonical": filename,
						},
					})
					if err != nil {
						slog.Error("Error while uploading the image", "objectId", objectId, "type", configuration.Type, "keyId", configuration.KeyID, "configuration", configuration.Name)
						continue
					}

					slog.Info("Proxy image created", "configuration", configuration.Name, "objectId", objectId)
				}

				_ = proxyCursor.Close(ctx)

				if message.Subject == "image.upload" {
					slog.Debug("Uploading new original image to S3", "objectId", objectId)
					_, err = s3.PutObject(ctx, bucket, filename, response, int64(responseAttrs.ObjectSize), minio.PutObjectOptions{
						ContentType: imageModel.MimeType,
						UserMetadata: map[string]string{
							"Image-From":       imageModel.KeyID.Hex(),
							"Image-Created-At": imageModel.ID.Timestamp().Format(time.RFC3339),
						},
					})
					if err != nil {
						slog.Warn("Error while loading the image to S3", "objectId", objectId)
						_ = message.NakWithDelay(500 * time.Millisecond)
						break
					}
				}

				_ = message.Ack()
				slog.Debug("Image proxy generation complete", "objectId", objectId)
				break
			case <-exitChannel:
				_ = subscribeUpload.Drain()
				_ = subscribeUpload.Unsubscribe()

				_ = subscribeGenerateProxy.Drain()
				_ = subscribeGenerateProxy.Unsubscribe()

				return
			}
		}
	}(processChannel, s.DB, s.Nats, s.S3)

	s.backgroundExitChannels = append(s.backgroundExitChannels, processChannel)
	return nil
}

func (s *ImageServiceWorker) Close() {
	for _, channel := range s.backgroundExitChannels {
		channel <- true
	}
}

type ProxyConfigurationServiceWorker struct {
	PublicBucket  string
	PrivateBucket string

	DB *mongo.Database

	Nats *nats.Conn

	backgroundExitChannels []chan bool
}

func (s *ProxyConfigurationServiceWorker) ForkBackgroundProcess() error {
	s.backgroundExitChannels = make([]chan bool, 0)

	processChannel := make(chan bool, 1)
	go func(exitChannel <-chan bool, db *mongo.Database, mq *nats.Conn) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		channel := make(chan *nats.Msg)
		subscribeUpload, err := mq.ChanSubscribe("image.proxy.update", channel)
		if err != nil {
			panic(err)
		}

		for {
			select {
			case message := <-channel:
				objectId, _ := primitive.ObjectIDFromHex(string(message.Data))

				collectionImages := db.Collection(services.ImagesCollection)
				imageCursor, err := collectionImages.Find(ctx, bson.M{"key_id": objectId})
				if err != nil {
					slog.Warn("Error while loading the images from DB", "objectId", objectId)
					_ = message.NakWithDelay(500 * time.Millisecond)
					break
				}

				for imageCursor.Next(ctx) {
					imageModel := models.Image{}
					if err := imageCursor.Decode(&imageModel); err != nil {
						slog.Warn("Error while decoding the image", "objectId", objectId)
						continue
					}

					slog.Debug("Forcing image rerender", "objectId", imageModel.ID, "keyId", objectId)
					_ = mq.Publish("image.generate-proxy", []byte(imageModel.ID.Hex()))
				}

				_ = imageCursor.Close(ctx)
			case <-exitChannel:
				_ = subscribeUpload.Drain()
				_ = subscribeUpload.Unsubscribe()

				return
			}
		}
	}(processChannel, s.DB, s.Nats)

	s.backgroundExitChannels = append(s.backgroundExitChannels, processChannel)
	return nil
}

func (s *ProxyConfigurationServiceWorker) Close() {
	for _, channel := range s.backgroundExitChannels {
		channel <- true
	}
}
