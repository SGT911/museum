package workers

type ServiceWorker interface {
	ForkBackgroundProcess() error
	Close()
}
