package main

import (
	connectcors "connectrpc.com/cors"
	"connectrpc.com/grpcreflect"
	"context"
	"errors"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"gitlab.com/SGT911/museum/backend/gen/museum/admin/v1/adminv1connect"
	"gitlab.com/SGT911/museum/backend/gen/museum/v1/museumv1connect"
	"gitlab.com/SGT911/museum/backend/services"
	"gitlab.com/SGT911/museum/backend/workers"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"log"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"
)

var serviceWorkers = make([]workers.ServiceWorker, 0)

func main() {
	// .env loading
	if err := godotenv.Load(); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			slog.Warn("No .env file found")
		} else {
			log.Fatal(err)
		}
	}

	slog.SetLogLoggerLevel(slog.LevelDebug)

	// Dependencies injection
	slog.Debug("Initializing NATS connection")
	natsClient, err := GetNatsClient()
	if err != nil {
		log.Fatal(err)
	}

	publicBucket := GetEnvOrDefault("S3_PUBLIC_BUCKET", "public")
	privateBucket := GetEnvOrDefault("S3_PUBLIC_BUCKET", "private")

	slog.Debug("Initializing S3 client")
	s3Client, err := GetS3Client()
	if err != nil {
		log.Fatal(err)
	}

	slog.Debug("Initializing MongoDB client")
	database, err := GetDatabase()
	if err != nil {
		log.Fatal(err)
	}

	tokenSecret := []byte(GetEnvOrDefault("MUSEUM_SECRET", "secret"))
	if string(tokenSecret) == "secret" {
		slog.Warn("Using default museum secret", "tokenSecret", "secret")
	}

	slog.Info("Initializing museum server")
	mux := http.NewServeMux()

	// Handlers
	mux.Handle(museumv1connect.NewPingServiceHandler(&services.PingService{}))

	imageService := &services.ImageService{
		PublicBucket:  publicBucket,
		PrivateBucket: privateBucket,

		DB: database,
		S3: s3Client,

		Nats: natsClient,
	}

	mux.Handle(museumv1connect.NewImageServiceHandler(imageService))
	mux.Handle(museumv1connect.NewImageProxyServiceHandler(imageService))
	mux.Handle(museumv1connect.NewAuthServiceHandler(&services.AuthService{
		DB: database,
	}))
	mux.Handle(adminv1connect.NewAdminAuthServiceHandler(&services.AdminAuthService{
		TokenSecret: tokenSecret,
		DB:          database,
	}))
	mux.Handle(adminv1connect.NewAdminKeyServiceHandler(&services.AdminKeyService{
		TokenSecret: tokenSecret,
		DB:          database,
	}))

	// Reflector service
	reflector := grpcreflect.NewStaticReflector(
		"museum.v1.PingService",
		"museum.v1.AuthService",

		"museum.v1.ImageService",
		"museum.v1.ImageProxyService",

		"museum.admin.v1.AdminAuthService",
		"museum.admin.v1.AdminKeyService",
	)

	mux.Handle(grpcreflect.NewHandlerV1(reflector))
	mux.Handle(grpcreflect.NewHandlerV1Alpha(reflector))

	middleware := cors.New(cors.Options{
		AllowedOrigins: strings.Split(GetEnvOrDefault("MUSEUM_ALLOWED_ORIGINS", "*"), ","),
		AllowedMethods: connectcors.AllowedMethods(),
		AllowedHeaders: connectcors.AllowedHeaders(),
		ExposedHeaders: connectcors.ExposedHeaders(),
	})

	// Listening server
	address := GetEnvOrDefault("MUSEUM_ADDRESS", ":8080")
	slog.Info("Starting server", "address", address)
	server := &http.Server{
		Addr:    address,
		Handler: middleware.Handler(h2c.NewHandler(mux, &http2.Server{})),
	}

	exitChan := make(chan os.Signal, 1)
	signal.Notify(exitChan, os.Interrupt)
	signal.Notify(exitChan, os.Kill)

	go func(server *http.Server) {
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatal(err)
		}
	}(server)

	slog.Debug("Initializing museum background workers")

	serviceWorkers = append(serviceWorkers, &workers.ImageServiceWorker{
		PublicBucket:  publicBucket,
		PrivateBucket: privateBucket,

		DB: database,
		S3: s3Client,

		Nats: natsClient,
	})

	serviceWorkers = append(serviceWorkers, &workers.ProxyConfigurationServiceWorker{
		PublicBucket:  publicBucket,
		PrivateBucket: privateBucket,

		DB:   database,
		Nats: natsClient,
	})

	for _, worker := range serviceWorkers {
		slog.Debug("Forking worker process", "worker", fmt.Sprintf("%T", worker))
		_ = worker.ForkBackgroundProcess()
	}

	<-exitChan
	ctx, cancel := context.WithTimeout(context.TODO(), 15*time.Second)
	defer cancel()

	slog.Debug("Shutting down server")
	_ = server.Shutdown(ctx)
	slog.Debug("Closing database")
	_ = database.Client().Disconnect(ctx)
	slog.Debug("Closing nats client")
	_ = natsClient.Drain()
	natsClient.Close()

	for _, worker := range serviceWorkers {
		worker.Close()
	}

	slog.Debug("Closing closing background workers")
}
